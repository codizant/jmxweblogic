package test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.naming.Context;
import javax.naming.InitialContext;

import com.oracle.jmx.eo.request.core.ServerConsoleTO;
import com.oracle.jmx.eo.response.datasource.DatasourceResponseTO;
import com.oracle.jmx.eo.response.datasource.DatasourceTO;
import com.oracle.jmx.util.common.JMXConection;
import com.oracle.jmx.ws.soap.datasource.DatasourceMonitorServiceImpl;

import weblogic.jndi.Environment;
import weblogic.management.MBeanHome;
import weblogic.management.configuration.AppDeploymentMBean;
import weblogic.management.configuration.DomainMBean;
import weblogic.management.jmx.MBeanServerInvocationHandler;
import weblogic.management.mbeanservers.domainruntime.DomainRuntimeServiceMBean;

public class DBAdapter {

	public static void main(String[] args) {
		String hostname = "192.168.56.101";
		String portString = "8001";
		String username = "weblogic";
		String password = "welcome1";
		ServerConsoleTO serverConsoleTO = new ServerConsoleTO();
		serverConsoleTO.setHostname(hostname);
		serverConsoleTO.setPortString(portString);
		serverConsoleTO.setUsername(username);
		serverConsoleTO.setPassword(password);
		DBAdapter datasourceMonitorServiceImpl = new DBAdapter();
		try {
			DatasourceResponseTO dsResponseTO = datasourceMonitorServiceImpl.getOutBoundConnection(serverConsoleTO);
			List<DatasourceTO> lstDatasourceTO = dsResponseTO.getLstDatasourceTO();
			/*
			 * for(DatasourceTO datasourceTO:lstDatasourceTO) {
			 * System.out.println("Jndiname--"+datasourceTO.getDatasourceGeneralTabTO().
			 * getDsName()); }
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private DatasourceResponseTO getOutBoundConnection(ServerConsoleTO serverConsoleTO) {
		Map jmxConnectionMap;
		try {
			jmxConnectionMap = JMXConection.initConnection(serverConsoleTO);
			MBeanServerConnection mBeanConn = (MBeanServerConnection) jmxConnectionMap.get("MBeanServerConnection");
			JMXConnector jmxConnector = (JMXConnector) jmxConnectionMap.get("JMXConnector");
			
			
			/*DomainRuntimeServiceMBean domainRuntimeServiceMBean = (DomainRuntimeServiceMBean) 
			   MBeanServerInvocationHandler.newProxyInstance(mBeanConn, new ObjectName(DomainRuntimeServiceMBean.OBJECT_NAME));       
			DomainMBean domainBean = domainRuntimeServiceMBean.getDomainConfiguration();
			System.out.println(domainBean.getDeployments());*/
			try {
				ObjectName  deploymentName = ObjectName
						.getInstance("com.bea.wlevs:Domain=" + "soa_domain" + ",Name=AppDeployment,Type=AppDeployment");
				AppDeploymentMBean deploymentMBean = (AppDeploymentMBean) MBeanServerInvocationHandler
						.newProxyInstance(mBeanConn, deploymentName, AppDeploymentMBean.class, true);
				System.out.println(deploymentMBean.getAbsolutePlanPath());
			} catch (MalformedObjectNameException | NullPointerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}

	

}
