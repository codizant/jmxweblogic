package test;

import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.naming.InitialContext;

import weblogic.jndi.Environment;

public class JMXHelper {
	private static JMXHelper instance = new JMXHelper();
	private String domainName;
	private String managedServerName;

	private JMXHelper() {
	}

	public static JMXHelper getInstance() {
		return instance;
	}

	public String getDomainName() {
		if (domainName == null) {
			try {
				MBeanServer server = getMBeanServer();
				ObjectName domainMBean = (ObjectName) server.getAttribute(getRuntimeService(), "DomainConfiguration");
				domainName = (String) server.getAttribute(domainMBean, "Name");
				System.out.println("nt DomainName: " + domainName);
			} catch (Exception ex) {
				System.out.println("Caught Exception: " + ex);
				ex.printStackTrace();
			}
		}
		return domainName;
	}

	public String getCurrentServerName() {
		if (managedServerName == null) {
			try {
				managedServerName = (String) getMBeanServer().getAttribute(getRuntimeService(), "ServerName");
				System.out.println("nt Current ServerName: " + managedServerName);
			} catch (Exception ex) {
				System.out.println("Caught Exception: " + ex);
				ex.printStackTrace();
			}
			return managedServerName;
		}
		return managedServerName;
	}

	public String getAdminServerURL() {
		String administrationURL = null;
		try {
			ObjectName msServerRuntime = new ObjectName("com.bea:Name=" + managedServerName + ",Type=ServerRuntime");
			administrationURL = (String) getMBeanServer().getAttribute(msServerRuntime, "AdminServerHost");
			int adminPort = (Integer) getMBeanServer().getAttribute(msServerRuntime, "AdminServerListenPort");
			administrationURL = administrationURL + ":" + adminPort;
			System.out.println("nt AdminServerURL: " + administrationURL);
		} catch (Exception ex) {
			System.out.println("Caught Exception: " + ex);
			ex.printStackTrace();
		}
		return administrationURL;
	}

	private MBeanServer getMBeanServer() {
		MBeanServer retval = null;
		InitialContext ctx = null;
		try {
			Properties properties = new Properties();
	        properties.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
	        properties.put("java.naming.provider.url", "t3://192.168.56.101:7001"); // works with IP address too
	        properties.put("java.naming.security.principal", "weblogic");
	        properties.put("java.naming.security.credentials", "welcome1");
	        ctx = new InitialContext(properties);
			
			retval = (MBeanServer) ctx.lookup("java:comp/env/jmx/runtime");
		} catch (Exception ex) {
			System.out.println("Caught Exception: " + ex);
			ex.printStackTrace();
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception dontCare) {
				}
			}
		}
		return retval;
	}

	private ObjectName getRuntimeService() {
		ObjectName retval = null;
		try {
			retval = new ObjectName(
					"com.bea:Name=RuntimeService,Type=weblogic.management.mbeanservers.runtime.RuntimeServiceMBean");
		} catch (Exception ex) {
			System.out.println("Caught Exception: " + ex);
			ex.printStackTrace();
		}
		return retval;
	}

	public static void main(String[] args) throws Exception {
		JMXHelper jmxHelper = new JMXHelper();
		jmxHelper.getDomainName();
	}
}
