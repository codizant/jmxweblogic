package test;
import com.bea.wli.config.Ref;
import com.bea.wli.monitoring.DomainMonitoringDisabledException;
import com.bea.wli.monitoring.InvalidServiceRefException;
import com.bea.wli.monitoring.MonitoringException;
import com.bea.wli.monitoring.MonitoringNotEnabledException;
import com.bea.wli.monitoring.ResourceStatistic;
import com.bea.wli.monitoring.ResourceType;
import com.bea.wli.monitoring.ServiceDomainMBean;
import com.bea.wli.monitoring.ServiceResourceStatistic;
import com.bea.wli.monitoring.StatisticType;
import com.bea.wli.monitoring.StatisticValue;
 
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
 
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
 
import java.net.MalformedURLException;
 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
 
import javax.naming.Context;
 
import weblogic.management.jmx.MBeanServerInvocationHandler;


public class ProxyServiceStatisticsRetriever {
	
	public  static ServiceDomainMBean initServiceDomainMBean(String host, int port, String username, String password) throws Exception {
		InvocationHandler handler = new ServiceDomainMBeanInvocationHandler(host, port, username, password);

		Object proxy = Proxy.newProxyInstance(ServiceDomainMBean.class.getClassLoader(),
				new Class[] { ServiceDomainMBean.class }, handler);

		ServiceDomainMBean serviceDomainMbean = (ServiceDomainMBean) proxy;
		return serviceDomainMbean;
	}
	public static class ServiceDomainMBeanInvocationHandler implements InvocationHandler {
        private String jndiURL =
            "weblogic.management.mbeanservers.domainruntime";
        private String mbeanName = ServiceDomainMBean.NAME;
        private String type = ServiceDomainMBean.TYPE;
 
        private String protocol = "t3";
        private String hostname = "192.168.56.101";
        private int port = 7001;
        private String jndiRoot = "/jndi/";
 
        private String username = "weblogic";
        private String password = "welcome1";
 
        private JMXConnector conn = null;
        private Object actualMBean = null;
 
        public ServiceDomainMBeanInvocationHandler(String hostName, int port,
                                                   String userName,
                                                   String password) {
            this.hostname = hostName;
            this.port = port;
            this.username = userName;
            this.password = password;
        }
 
        /**
         * Gets JMX connection
         */
        public JMXConnector initConnection() throws IOException,
                                                    MalformedURLException {
            JMXServiceURL serviceURL =
                new JMXServiceURL(protocol, hostname, port,
                                  jndiRoot + jndiURL);
            Hashtable h = new Hashtable();
 
            if (username != null)
                h.put(Context.SECURITY_PRINCIPAL, username);
            if (password != null)
                h.put(Context.SECURITY_CREDENTIALS, password);
 
            h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES,
                  "weblogic.management.remote");
 
            return JMXConnectorFactory.connect(serviceURL, h);
        }
 
        /**
         * Invokes specified method with specified params on specified
         * object.
         */
        public Object invoke(Object proxy, Method method,
                             Object[] args) throws Throwable {
            if (conn == null)
                conn = initConnection();
 
            if (actualMBean == null)
                actualMBean =
                        findServiceDomain(conn.getMBeanServerConnection(),
                                          mbeanName, type, null);
 
            return method.invoke(actualMBean, args);
        }
 
        /**
         * Finds the specified MBean object
         *
         * @param connection - A connection to the MBeanServer.
         * @param mbeanName  - The name of the MBean instance.
         * @param mbeanType  - The type of the MBean.
         * @param parent     - The name of the parent Service. Can be NULL.
         * @return Object - The MBean or null if the MBean was not found.
         */
        public Object findServiceDomain(MBeanServerConnection connection,
                                        String mbeanName, String mbeanType,
                                        String parent) {
            try {
                ObjectName on = new ObjectName(ServiceDomainMBean.OBJECT_NAME);
                return (ServiceDomainMBean)MBeanServerInvocationHandler.newProxyInstance(connection,
                                                                                         on);
            } catch (MalformedObjectNameException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
	public static void main(String[] args) throws IOException, MalformedURLException, Exception {  
		String hostname = "192.168.56.101";
		int port = 7001;
		String username = "weblogic";
		String password = "welcome1";
		getAndResetStatsForAllMonitoredProxyServices(hostname,  port,  username,  password);
	}
	public static void getAndResetStatsForAllMonitoredProxyServices(String host, int port, String username, String password) throws Exception {
		ServiceDomainMBean serviceDomainMbean=ProxyServiceStatisticsRetriever.initServiceDomainMBean(host, port, username, password);
        Ref[] serviceRefs =
             serviceDomainMbean.getMonitoredProxyServiceRefs();
        // Create a bitwise map for desired resource types.
        int typeFlag = 0;
        typeFlag = typeFlag | ResourceType.SERVICE.value();
        typeFlag = typeFlag | ResourceType.FLOW_COMPONENT.value();
        typeFlag = typeFlag | ResourceType.WEBSERVICE_OPERATION.value();

        HashMap<Ref, ServiceResourceStatistic> resourcesMap = null;
        
        // Get cluster-level statistics.
        try {
             // Get statistics.
             System.out.println("Now trying to get statistics for -" +
                  serviceRefs.length + " proxy services...");
             resourcesMap = serviceDomainMbean.getProxyServiceStatistics
                  (serviceRefs,typeFlag, null);

             // Reset statistics.
             long resetRequestTime =
                  serviceDomainMbean.resetStatistics(serviceRefs);

             // Save retrieved statistics.
             String fileName = "ProxyStatistics_" +
                  new SimpleDateFormat("yyyy_MM_dd_HH_mm").
                       format(new Date(System.currentTimeMillis())) +
                            ".txt";
             saveStatisticsToFile(resourcesMap, resetRequestTime,
                  fileName);
        }
        catch (IllegalArgumentException iae) {
             System.out.println("===================================\n");
            System.out.println("Encountered IllegalArgumentException...Details:");
            System.out.println(iae.getMessage());
                System.out.println("Check if business ref was passed OR bitmap is " + "invalid...\nIf so correct it and try again!!!");
             System.out.println("===================================\n");
                  throw iae;
        }


        catch (DomainMonitoringDisabledException dmde) {
             /** Statistics not available as monitoring is turned off at the
            * domain level.
            */
             System.out.println("===================================\n");
            System.out.println("Statistics not available as monitoring" +
                  "is turned off at domain level.");
             System.out.println("===================================\n");
            throw dmde;
        }
        catch (MonitoringException me) {
             // Internal problem ... May be aggregation server is              crashed ...
             System.out.println("===================================\n");
              System.out.println("ERROR: Statistics is not available... " +
                  "Check if aggregation server is crashed...");
             System.out.println("===================================\n");
            throw me;
        }
   }
	  private static  void saveStatisticsToFile(
	          HashMap<Ref, ServiceResourceStatistic> statsMap,
	               long resetReqTime, String fileName) throws Exception {
	          if (statsMap == null) {
	               System.out.println("\nService statistics map is null...Nothing to save.\n");
	          }
	          if (statsMap.size() == 0) {
	               System.out.println("\nService statistics map is empty...Nothing to save.\n");
	          }
	          FileWriter out = new FileWriter(new File(fileName));

	          out.write("*********************************************\n");
	          out.write("This file contains statistics for " + statsMap.size()
	               + " services.\n");
	          out.write("***********************************************\n");

	          Set<Map.Entry<Ref, ServiceResourceStatistic>> set =
	               statsMap.entrySet();
	          
	          System.out.println(new StringBuffer().append("\nWriting stats to the file - ").append(fileName).append("\n").toString());

	          // Print statistical information of each service
	          for (Map.Entry<Ref, ServiceResourceStatistic> mapEntry : set) {
	               out.write(new StringBuffer().
	                        append("\n\n======= Pirnting statistics for service ").
	                    append(mapEntry.getKey().getFullName()).
	                    append("=======\n").toString());

	                   ServiceResourceStatistic serviceStats = mapEntry.getValue();
	               out.write(new StringBuffer().
	                    append("Statistic collection time is - ").
	                    append(new Date(serviceStats.getCollectionTimestamp
	                         ())).
	                    append("\n").toString());

	               ResourceStatistic[] resStatsArray = null;
	               try {
	                    resStatsArray = serviceStats.getAllResourceStatistics
	                         ();
	               }
	               catch (MonitoringNotEnabledException mnee) {
	                    // Statistics not available
	                    out.write("WARNING: Monitoring is not enabled for  " +
	                         "this service... Do someting...");
	                    out.write("=====================================\n");
	                    continue;
	               }
	               
	               catch (InvalidServiceRefException isre) {
	                    // Invalid service
	                    out.write("ERROR: Invlaid Ref. May be this service is" +
	                         "deleted. Do something...");
	                    out.write("======================================\n");
	                    continue;
	               }

	               catch (MonitoringException me) {
	                    // Statistics not available
	                    out.write("ERROR: Failed to get statistics for this service... " + "Details: " + me.getMessage());
	                    me.printStackTrace();
	                    out.write("======================================\n");
	                    continue;
	               }
	               for (ResourceStatistic resStats : resStatsArray) {
	                    // Print resource information
	                    out.write("\nResource name: " + resStats.getName());
	                    out.write("\tResource type: " +
	                         resStats.getResourceType().toString());

	                    // Now get and print statistics for this resource
	                          StatisticValue[] statValues = resStats.getStatistics();
	                    for (StatisticValue value : statValues) {
	                         out.write("\n\t\tStatistic Name - " +
	                              value.getName ());
	                         out.write("\n\t\tStatistic Type - " +
	                              value.getType().toString());

	                         // Determine statistics type
	                           if ( value.getType() == StatisticType.INTERVAL ) {
	                              StatisticValue.IntervalStatistic is =
	                               (StatisticValue.IntervalStatistic)value;

	                              // Print interval statistics values
	                              out.write("\n\t\t\t\tCount Value - " + is.getCount());
	                              out.write("\n\t\t\t\tMin Value - " +
	                                    is.getMin());
	                              out.write("\n\t\t\t\tMax Value - " +
	                                    is.getMax());
	                              out.write("\n\t\t\t\tSum Value - " +
	                                    is.getSum());
	                              out.write("\n\t\t\t\tAve Value - " +
	                                    is.getAverage());
	                         }
	                         else if ( value.getType() == StatisticType.
	                              COUNT ) {
	                              StatisticValue.CountStatistic cs =
	                                   (StatisticValue.CountStatistic)
	                                        value;

	                              // Print count statistics value
	                              out.write("\n\t\t\t\tCount Value - " +
	                                    cs.getCount());
	                         }
	                         else if ( value.getType() == StatisticType.STATUS
	                              ){
	                              StatisticValue.StatusStatistic ss =
	                                   (StatisticValue.StatusStatistic)value;
	                              // Print count statistics value
	                              out.write("\n\t\t\t\t Initial Status - " +
	                                    ss.getInitialStatus());
	                              out.write("\n\t\t\t\t Current Status - " +
	                                    ss.getCurrentStatus());

	                         }
	                    }
	               }
	               out.write("\n=========================================\n");
	          }
	          if (resetReqTime > 0) {
	               // Save reset request time.
	               out.write("\n*****************************************\n");
	                 out.write("Statistics for all these services are RESET.\n");
	               out.write("RESET request time is " +
	                    new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").
	                         format(new Date(resetReqTime)));
	               out.write("\n****************************************\n");
	          }

	          // Flush and close file.
	          out.flush();
	          out.close();
	     }
}
