package test;

import java.io.IOException;  
import java.net.MalformedURLException;  
import java.util.ArrayList;  
import java.util.Hashtable;  
import java.util.Set;  
import javax.management.MBeanServer;  
import javax.management.MBeanServerConnection;  
import javax.management.ObjectName;  
import javax.management.openmbean.CompositeDataSupport;  
import javax.management.remote.JMXConnector;  
import javax.management.remote.JMXConnectorFactory;  
import javax.management.remote.JMXServiceURL;  
import javax.naming.Context;  
import javax.naming.InitialContext;  
import javax.naming.NamingException;  
import weblogic.management.mbeanservers.domainruntime.DomainRuntimeServiceMBean;  
public class SBEndpointList {  
  private static MBeanServerConnection connection;  
  private static String HOST = "192.168.56.101";  
  private static Integer PORT = 7001;  
  private static String USERNAME = "weblogic";  
  private static String PASSWORD = "welcome1";  
  public SBEndpointList() throws IOException, MalformedURLException, NamingException {  
    this.connection = this.getMBeanServerConnection();  
  }  
  private ObjectName[] getObjectNames(ObjectName base, String child) throws Exception {  
    return (ObjectName[]) connection.getAttribute(base, child);  
  }  
  public ArrayList<String> getEndpoints() throws Exception {  
    ArrayList<String> result = new ArrayList<String>();  
    Set<ObjectName> osbconfigs =  
      connection.queryNames(new ObjectName("com.oracle.osb:Type=ResourceConfigurationMBean,*"), null);  
    for (ObjectName config : osbconfigs) {
      System.out.println("config.getKeyProperty(\"Name\")===>"+config.getKeyProperty("Name"));
      if (config.getKeyProperty("Name").startsWith("ProxyService$")) {  
        //System.out.println(config);  
        CompositeDataSupport cds = (CompositeDataSupport) connection.getAttribute(config, "Configuration");  
        String servicetype = (String) cds.get("service-type"); 
        System.out.println("servicetype===>"+servicetype);
        if (servicetype.equals("SOAP")) {  
          CompositeDataSupport pstt = (CompositeDataSupport) cds.get("transport-configuration");  
          String url = (String) pstt.get("url");  
          String tt = (String) pstt.get("transport-type");  
          if (tt.equals("http")) {  
            result.add("http://SERVER:PORT" +("/" + url).replace("//" ,"/") );  
          }  
        }  
      }  
    }  
    return result;  
  }  
  private JMXConnector initRemoteConnection(String hostname, int port, String username,  
                       String password) throws IOException, MalformedURLException {  
    JMXServiceURL serviceURL =  
      new JMXServiceURL("t3", hostname, port, "/jndi/" + DomainRuntimeServiceMBean.MBEANSERVER_JNDI_NAME);  
    Hashtable<String, String> h = new Hashtable<String, String>();  
    h.put(Context.SECURITY_PRINCIPAL, username);  
    h.put(Context.SECURITY_CREDENTIALS, password);  
    h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");  
    return JMXConnectorFactory.connect(serviceURL, h);  
  }  
  private MBeanServerConnection getMBeanServerConnection() throws IOException, MalformedURLException,  
                                  NamingException {  
    try {  
      InitialContext ctx = new InitialContext();  
      MBeanServer server = (MBeanServer) ctx.lookup("java:comp/env/jmx/runtime");  
      return server;  
    } catch (Exception e) {  
      JMXConnector jmxcon = initRemoteConnection(HOST, PORT, USERNAME, PASSWORD);  
      return jmxcon.getMBeanServerConnection();  
    }  
  }  
  public static void main(String[] args) throws IOException, MalformedURLException, Exception {  
    SBEndpointList me = new SBEndpointList();  
    ArrayList<String> result = me.getEndpoints();  
    System.out.println("Endpoints:");  
    for (String endpoint : result) {  
      System.out.println(endpoint);  
    }  
  }  
}  
