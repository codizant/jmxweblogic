package com.oracle.jmx.eo.response.datasource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "DATASOURCE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DATASOURCE")
public class DatasourceTO {
	@XmlElement(name = "DATASOURCEGENETALTAB")
	DatasourceGeneralTabTO datasourceGeneralTabTO;
	@XmlElement(name = "DATASOURCECONNECTIONPOOLTAB")
	DatasourceConnectionPoolTabTO datasourceConnectionPoolTabTO;
	@XmlElement(name = "DATASOURCETARGETS")
	DatasourceTargetTO datasourceTargetTO;
	/*String dsConnectionsTotalCount;
	String dsCurrCapacity;
	String dsActiveConnectionsAverageCount;
	String dsActiveConnectionsCurrentCount;
	String dsActiveConnectionsHighCount;
	String dsConnectionDelayTime;
	String dsCurrCapacityHighCount;
	String dsFailedReserveRequestCount;
	String dsHighestNumAvailable;
	String dsLeakedConnectionCount;
	String dsNumAvailable;
	String dsNumUnavailable;
	String dsWaitingForConnectionCurrentCount;
	String dsWaitingForConnectionFailureTotal;
	String dsWaitingForConnectionHighCount;
	String dsWaitingForConnectionSuccessTotal;
	String dsWaitingForConnectionTotal;
	String dsWaitSecondsHighCount;*/
	
	
	public DatasourceGeneralTabTO getDatasourceGeneralTabTO() {
		return datasourceGeneralTabTO;
	}
	public void setDatasourceGeneralTabTO(DatasourceGeneralTabTO datasourceGeneralTabTO) {
		this.datasourceGeneralTabTO = datasourceGeneralTabTO;
	}
	public DatasourceConnectionPoolTabTO getDatasourceConnectionPoolTabTO() {
		return datasourceConnectionPoolTabTO;
	}
	public void setDatasourceConnectionPoolTabTO(DatasourceConnectionPoolTabTO datasourceConnectionPoolTabTO) {
		this.datasourceConnectionPoolTabTO = datasourceConnectionPoolTabTO;
	}
	public DatasourceTargetTO getDatasourceTargetTO() {
		return datasourceTargetTO;
	}
	public void setDatasourceTargetTO(DatasourceTargetTO datasourceTargetTO) {
		this.datasourceTargetTO = datasourceTargetTO;
	}
	
	
	/*public String getDsConnectionsTotalCount() {
		return dsConnectionsTotalCount;
	}
	public void setDsConnectionsTotalCount(String dsConnectionsTotalCount) {
		this.dsConnectionsTotalCount = dsConnectionsTotalCount;
	}
	public String getDsCurrCapacity() {
		return dsCurrCapacity;
	}
	public void setDsCurrCapacity(String dsCurrCapacity) {
		this.dsCurrCapacity = dsCurrCapacity;
	}
	public String getDsActiveConnectionsAverageCount() {
		return dsActiveConnectionsAverageCount;
	}
	public void setDsActiveConnectionsAverageCount(String dsActiveConnectionsAverageCount) {
		this.dsActiveConnectionsAverageCount = dsActiveConnectionsAverageCount;
	}
	public String getDsActiveConnectionsCurrentCount() {
		return dsActiveConnectionsCurrentCount;
	}
	public void setDsActiveConnectionsCurrentCount(String dsActiveConnectionsCurrentCount) {
		this.dsActiveConnectionsCurrentCount = dsActiveConnectionsCurrentCount;
	}
	public String getDsActiveConnectionsHighCount() {
		return dsActiveConnectionsHighCount;
	}
	public void setDsActiveConnectionsHighCount(String dsActiveConnectionsHighCount) {
		this.dsActiveConnectionsHighCount = dsActiveConnectionsHighCount;
	}
	public String getDsConnectionDelayTime() {
		return dsConnectionDelayTime;
	}
	public void setDsConnectionDelayTime(String dsConnectionDelayTime) {
		this.dsConnectionDelayTime = dsConnectionDelayTime;
	}
	public String getDsCurrCapacityHighCount() {
		return dsCurrCapacityHighCount;
	}
	public void setDsCurrCapacityHighCount(String dsCurrCapacityHighCount) {
		this.dsCurrCapacityHighCount = dsCurrCapacityHighCount;
	}
	public String getDsFailedReserveRequestCount() {
		return dsFailedReserveRequestCount;
	}
	public void setDsFailedReserveRequestCount(String dsFailedReserveRequestCount) {
		this.dsFailedReserveRequestCount = dsFailedReserveRequestCount;
	}
	public String getDsHighestNumAvailable() {
		return dsHighestNumAvailable;
	}
	public void setDsHighestNumAvailable(String dsHighestNumAvailable) {
		this.dsHighestNumAvailable = dsHighestNumAvailable;
	}
	public String getDsLeakedConnectionCount() {
		return dsLeakedConnectionCount;
	}
	public void setDsLeakedConnectionCount(String dsLeakedConnectionCount) {
		this.dsLeakedConnectionCount = dsLeakedConnectionCount;
	}
	public String getDsNumAvailable() {
		return dsNumAvailable;
	}
	public void setDsNumAvailable(String dsNumAvailable) {
		this.dsNumAvailable = dsNumAvailable;
	}
	public String getDsNumUnavailable() {
		return dsNumUnavailable;
	}
	public void setDsNumUnavailable(String dsNumUnavailable) {
		this.dsNumUnavailable = dsNumUnavailable;
	}
	public String getDsWaitingForConnectionCurrentCount() {
		return dsWaitingForConnectionCurrentCount;
	}
	public void setDsWaitingForConnectionCurrentCount(String dsWaitingForConnectionCurrentCount) {
		this.dsWaitingForConnectionCurrentCount = dsWaitingForConnectionCurrentCount;
	}
	public String getDsWaitingForConnectionFailureTotal() {
		return dsWaitingForConnectionFailureTotal;
	}
	public void setDsWaitingForConnectionFailureTotal(String dsWaitingForConnectionFailureTotal) {
		this.dsWaitingForConnectionFailureTotal = dsWaitingForConnectionFailureTotal;
	}
	public String getDsWaitingForConnectionHighCount() {
		return dsWaitingForConnectionHighCount;
	}
	public void setDsWaitingForConnectionHighCount(String dsWaitingForConnectionHighCount) {
		this.dsWaitingForConnectionHighCount = dsWaitingForConnectionHighCount;
	}
	public String getDsWaitingForConnectionSuccessTotal() {
		return dsWaitingForConnectionSuccessTotal;
	}
	public void setDsWaitingForConnectionSuccessTotal(String dsWaitingForConnectionSuccessTotal) {
		this.dsWaitingForConnectionSuccessTotal = dsWaitingForConnectionSuccessTotal;
	}
	public String getDsWaitingForConnectionTotal() {
		return dsWaitingForConnectionTotal;
	}
	public void setDsWaitingForConnectionTotal(String dsWaitingForConnectionTotal) {
		this.dsWaitingForConnectionTotal = dsWaitingForConnectionTotal;
	}
	public String getDsWaitSecondsHighCount() {
		return dsWaitSecondsHighCount;
	}
	public void setDsWaitSecondsHighCount(String dsWaitSecondsHighCount) {
		this.dsWaitSecondsHighCount = dsWaitSecondsHighCount;
	}*/
	
	

}
