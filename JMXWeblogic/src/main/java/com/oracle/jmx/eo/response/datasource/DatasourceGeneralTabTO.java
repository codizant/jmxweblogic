package com.oracle.jmx.eo.response.datasource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlRootElement(name = "DATASOURCEGENETALTAB")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DATASOURCEGENETALTAB")
public class DatasourceGeneralTabTO {
	@XmlElement(name = "DATASOURCENAME")
	String dsName;
	@XmlElement(name = "DATASOURCEJNDINAMES")
	DatasourceJNDITO dsJNDINames;
	@XmlElement(name = "DATASOURCESTATE")
	String dsState;
	public String getDsName() {
		return dsName;
	}
	public void setDsName(String dsName) {
		this.dsName = dsName;
	}
	
	public DatasourceJNDITO getDsJNDINames() {
		return dsJNDINames;
	}
	public void setDsJNDINames(DatasourceJNDITO dsJNDINames) {
		this.dsJNDINames = dsJNDINames;
	}
	public String getDsState() {
		return dsState;
	}
	public void setDsState(String dsState) {
		this.dsState = dsState;
	}

	

}
