package com.oracle.jmx.eo.response.servicebus.components;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "SERVICEBUSRESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SERVICEBUSRESPONSE")
public class ServicebusResponseTO {
	@XmlElement(name = "SERVICEBUSARTIFACTS")
	List<ServicebusComponentsTO> lstservicebusartifactsTO;

	public List<ServicebusComponentsTO> getLstservicebusartifactsTO() {
		return lstservicebusartifactsTO;
	}

	public void setLstservicebusartifactsTO(List<ServicebusComponentsTO> lstservicebusartifactsTO) {
		this.lstservicebusartifactsTO = lstservicebusartifactsTO;
	}

	
}
