package com.oracle.jmx.eo.response.datasource;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;
@XmlRootElement(name = "DATASOURCECONNECTIONPOOLTAB")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DATASOURCECONNECTIONPOOLTAB")
public class DatasourceConnectionPoolTabTO {
	@XmlElement(name = "DRIVERNAME")
	String dsDriverName;
	@XmlElement(name = "XAENABLED")
	boolean xaEnabled;
	@XmlElement(name = "URL")
	String dsUrl;
	@XmlElement(name = "USERNAME")
	String dsUsername;
	@XmlElement(name = "INITIALCAPACITY")
	int initialCapacity;
	@XmlElement(name = "MAXCAPACITY")
	int maxCapacity;
	@XmlElement(name = "MINCAPACITY")
	int minCapacity;
	public String getDsDriverName() {
		return dsDriverName;
	}
	public void setDsDriverName(String dsDriverName) {
		this.dsDriverName = dsDriverName;
	}
	public boolean isXaEnabled() {
		return xaEnabled;
	}
	public void setXaEnabled(boolean xaEnabled) {
		this.xaEnabled = xaEnabled;
	}
	public String getDsUrl() {
		return dsUrl;
	}
	public void setDsUrl(String dsUrl) {
		this.dsUrl = dsUrl;
	}
	public String getDsUsername() {
		return dsUsername;
	}
	public void setDsUsername(String dsUsername) {
		this.dsUsername = dsUsername;
	}
	public int getInitialCapacity() {
		return initialCapacity;
	}
	public void setInitialCapacity(int initialCapacity) {
		this.initialCapacity = initialCapacity;
	}
	public int getMaxCapacity() {
		return maxCapacity;
	}
	public void setMaxCapacity(int maxCapacity) {
		this.maxCapacity = maxCapacity;
	}
	public int getMinCapacity() {
		return minCapacity;
	}
	public void setMinCapacity(int minCapacity) {
		this.minCapacity = minCapacity;
	}
	
	
	 

	

}
