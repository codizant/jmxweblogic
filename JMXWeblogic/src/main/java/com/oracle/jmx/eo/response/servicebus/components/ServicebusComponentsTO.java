package com.oracle.jmx.eo.response.servicebus.components;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ServicebusComponentsTO {
	@XmlElement(name = "APPLICATIONNAME")
	private String applicationName;
	@XmlElement(name = "SERVICETYPE")
	private String serviceType;
	@XmlElement(name = "SERVICENAME")
	private String serviceName;
	@XmlElement(name = "SERVICEPATH")
	private String fullServicePath;
	@XmlElement(name = "SERVICESTATUS")
	private String serviceStatus;
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getFullServicePath() {
		return fullServicePath;
	}
	public void setFullServicePath(String fullServicePath) {
		this.fullServicePath = fullServicePath;
	}
	public String getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}
	
	

}
