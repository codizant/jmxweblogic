package com.oracle.jmx.eo.request.core;

public class ServerConsoleTO {
	private String hostname;
	private String portString;
	private String username;
	private String password;
	
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getPortString() {
		return portString;
	}
	public void setPortString(String portString) {
		this.portString = portString;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
