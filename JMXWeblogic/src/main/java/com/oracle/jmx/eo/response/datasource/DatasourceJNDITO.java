package com.oracle.jmx.eo.response.datasource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "DATASOURCEJNDINAMES")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DATASOURCEJNDINAMES")
public class DatasourceJNDITO {
	@XmlElement(name = "DATASOURCEJNDINAME")
	String[] dsJNDINames;

	public String[] getDsJNDINames() {
		return dsJNDINames;
	}

	public void setDsJNDINames(String[] dsJNDINames) {
		this.dsJNDINames = dsJNDINames;
	}
	
}
