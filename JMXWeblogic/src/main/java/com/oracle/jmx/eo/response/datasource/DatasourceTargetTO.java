package com.oracle.jmx.eo.response.datasource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlRootElement(name = "DATASOURCETARGETS")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DATASOURCETARGET")
public class DatasourceTargetTO {
	@XmlElement(name = "DATASOURCETARGET")
	String[] datasourceTargetServer;

	public String[] getDatasourceTargetServer() {
		return datasourceTargetServer;
	}

	public void setDatasourceTargetServer(String[] datasourceTargetServer) {
		this.datasourceTargetServer = datasourceTargetServer;
	}
	
}
