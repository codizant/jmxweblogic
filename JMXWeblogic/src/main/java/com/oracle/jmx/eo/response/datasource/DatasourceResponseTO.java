package com.oracle.jmx.eo.response.datasource;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "DATASOURCERESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DATASOURCERESPONSE")
public class DatasourceResponseTO {
	@XmlElement(name = "DATASOURCERESPONSE")
	List<DatasourceTO> lstDatasourceTO;

	public List<DatasourceTO> getLstDatasourceTO() {
		return lstDatasourceTO;
	}

	public void setLstDatasourceTO(List<DatasourceTO> lstDatasourceTO) {
		this.lstDatasourceTO = lstDatasourceTO;
	}
	

}
