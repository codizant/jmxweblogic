package com.oracle.jmx.core;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

import com.oracle.jmx.eo.request.core.ServerConsoleTO;

public class ConnectWLS {

	// Weblogic JMX Connections	
	
	private static final ObjectName service = null;
	ObjectName[] atnProviders = null;
	ObjectName mBeanTypeService = null;

	// This method creates a WebLogic connection Object
	

	// This method prints WebLogic serverRuntime Properties
	public static void displayWeblogicServerArguments(MBeanServerConnection mbeanConn) throws Exception {
		ObjectName[] serverRT = getServerRuntimes(mbeanConn);
		System.out.println("got server runtimes");
		int length = (int) serverRT.length;
		for (int i = 0; i < length; i++) {
			String name = (String) mbeanConn.getAttribute(serverRT[i], "Name");
			String state = (String) mbeanConn.getAttribute(serverRT[i], "State");
			String weblogicHome = (String) mbeanConn.getAttribute(serverRT[i], "WeblogicHome");
			String DefaultURL = (String) mbeanConn.getAttribute(serverRT[i], "DefaultURL");
			String SSLListenAddress = (String) mbeanConn.getAttribute(serverRT[i], "SSLListenAddress");
			String CurrentDirectory = (String) mbeanConn.getAttribute(serverRT[i], "CurrentDirectory");
			String AdministrationURL = (String) mbeanConn.getAttribute(serverRT[i], "AdministrationURL");

			System.out.println("Server name = " + name + ".   Server state: " + state);
			System.out.println("WebLogic Home = " + weblogicHome);
			System.out.println("DefaultURL = " + DefaultURL);
			System.out.println("SSL Listen Address = " + SSLListenAddress);
			System.out.println("Current Directory = " + CurrentDirectory);
			System.out.println("Administration URL = " + AdministrationURL);

		}
	}

	// WebLogic ServerRuntime
	public static ObjectName[] getServerRuntimes(MBeanServerConnection mbeanConn ) throws Exception {
		ObjectName service = new ObjectName("com.bea:Name=DomainRuntimeService,Type=weblogic.management."
				+ "mbeanservers.domainruntime.DomainRuntimeServiceMBean");
		return (ObjectName[]) mbeanConn.getAttribute(service, "ServerRuntimes");
	}

	// Shutdown the WebLogic Server
	private static void shutDownServer(MBeanServerConnection mbeanConn) throws Exception {
		// TODO Auto-generated method stub
		ObjectName[] serverRT = getServerRuntimes(mbeanConn);
		System.out.println("got server runtimes");
		int length = (int) serverRT.length;
		for (int i = 0; i < length; i++) {
			mbeanConn.invoke(serverRT[i], "forceShutdown", null, null);

		}
	}

	

}
