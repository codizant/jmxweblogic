package com.oracle.jmx.ws.soap.datasource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;

import com.oracle.jmx.core.ConnectWLS;
import com.oracle.jmx.eo.request.core.ServerConsoleTO;
import com.oracle.jmx.eo.response.datasource.DatasourceConnectionPoolTabTO;
import com.oracle.jmx.eo.response.datasource.DatasourceGeneralTabTO;
import com.oracle.jmx.eo.response.datasource.DatasourceJNDITO;
import com.oracle.jmx.eo.response.datasource.DatasourceResponseTO;
import com.oracle.jmx.eo.response.datasource.DatasourceTO;
import com.oracle.jmx.eo.response.datasource.DatasourceTargetTO;
import com.oracle.jmx.util.common.JMXConection;
import com.oracle.jmx.util.common.JMXWrapperRemote;
import com.oracle.jmx.util.datasource.DatasourceUtil;

import weblogic.management.jmx.MBeanServerInvocationHandler;
import weblogic.management.runtime.JDBCDataSourceRuntimeMBean;

@WebService(endpointInterface = "com.oracle.jmx.ws.soap.datasource.IDatasourceMonitorService")
public class DatasourceMonitorServiceImpl implements IDatasourceMonitorService {

	public DatasourceResponseTO getDataSources(ServerConsoleTO serverConsoleTO) throws Exception {
		DatasourceResponseTO datasourceResponseTo = new DatasourceResponseTO();
		List<DatasourceTO> lstDatasourceTO = new ArrayList();
		DatasourceTO datasourceTO = new DatasourceTO();
		DatasourceJNDITO datasourceJNDITO = new DatasourceJNDITO();
		DatasourceTargetTO dsTargetTo = new DatasourceTargetTO();
		Map jmxConnectionMap = JMXConection.initConnection(serverConsoleTO);
		MBeanServerConnection mBeanConn = (MBeanServerConnection) jmxConnectionMap.get("MBeanServerConnection");
		JMXConnector jmxConnector = (JMXConnector) jmxConnectionMap.get("JMXConnector");
		DatasourceGeneralTabTO datasourceGeneralTabTO = new DatasourceGeneralTabTO();
		DatasourceConnectionPoolTabTO datasourceConnectionPoolTabTO = new DatasourceConnectionPoolTabTO();
		ObjectName[] serverRT = DatasourceUtil.getServerRuntimes(mBeanConn);
		String dsName = "";
		for (int i = 0; i < serverRT.length; i++) {
			System.out.println("Server Instance=" + serverRT[i]);
			String serverName = (String) mBeanConn.getAttribute(serverRT[i], "Name");
			ObjectName[] connectionPools = DatasourceUtil.getListDS(serverName, mBeanConn);
			int pool_length = (int) connectionPools.length;
			for (int x = 0; x < pool_length; x++) {
				dsName = (String) mBeanConn.getAttribute(connectionPools[x], "Name");
				datasourceGeneralTabTO.setDsName(dsName);
				System.out.println("Name = " + dsName);
				datasourceGeneralTabTO.setDsState((String) mBeanConn.getAttribute(connectionPools[x], "State"));

				ObjectName jdbcSystemResource = new ObjectName("com.bea:Name=" + dsName + ",Type=JDBCSystemResource");
				ObjectName[] jdbcObjTargets = (ObjectName[]) mBeanConn.getAttribute(jdbcSystemResource, "Targets");

				List<String> jdbcTargets = new <String>ArrayList();

				for (ObjectName servername : jdbcObjTargets) {
					String servecanonicalName = servername.getCanonicalName();
					System.out.println("servecanonicalName = " + servecanonicalName);
					jdbcTargets.add(servecanonicalName.substring(servecanonicalName.indexOf("com.bea:Name=") + 13,
							servecanonicalName.indexOf(",Type=Server")));
				}
				String datasourceTargets[] = jdbcTargets.toArray(new String[jdbcTargets.size()]);
				dsTargetTo.setDatasourceTargetServer(datasourceTargets);

				ObjectName JDBCResource = (ObjectName) mBeanConn.getAttribute(jdbcSystemResource, "JDBCResource");

				ObjectName jdbcDataSourceParams = (ObjectName) mBeanConn.getAttribute(JDBCResource,
						"JDBCDataSourceParams");
				String jndiNames[] = (String[]) mBeanConn.getAttribute(jdbcDataSourceParams, "JNDINames");
				for (String strjndiNames : jndiNames) {
					System.out.println("JNDINames = " + strjndiNames);
				}
				datasourceJNDITO.setDsJNDINames(jndiNames);
				datasourceGeneralTabTO.setDsJNDINames(datasourceJNDITO);
				datasourceJNDITO = new DatasourceJNDITO();
				ObjectName jdbcDriverParams = (ObjectName) mBeanConn.getAttribute(JDBCResource, "JDBCDriverParams");
				ObjectName jdbcPropertiesBean = DatasourceUtil.getJDBCSystemProperties(mBeanConn, jdbcDriverParams);
				ObjectName jdbcUserPropertyBean = DatasourceUtil.getJDBCUserPropertyBean(mBeanConn, jdbcPropertiesBean);

				String URL = (String) mBeanConn.getAttribute(jdbcDriverParams, "Url");
				String dbusername = (String) DatasourceUtil.getObjectName(mBeanConn, jdbcUserPropertyBean, "Value");
				String DriverName = (String) mBeanConn.getAttribute(jdbcDriverParams, "DriverName");
				boolean isXAEnabled = (boolean) mBeanConn.getAttribute(jdbcDriverParams, "UseXaDataSourceInterface");

				ObjectName jdbcConnectionPoolParamsMBean = (ObjectName) mBeanConn.getAttribute(JDBCResource,
						"JDBCConnectionPoolParams");
				Integer initialCapacity = (Integer) DatasourceUtil.getObjectName(mBeanConn,
						jdbcConnectionPoolParamsMBean, "InitialCapacity");
				Integer maxCapacity = (Integer) DatasourceUtil.getObjectName(mBeanConn, jdbcConnectionPoolParamsMBean,
						"MaxCapacity");
				Integer minCapacity = (Integer) DatasourceUtil.getObjectName(mBeanConn, jdbcConnectionPoolParamsMBean,
						"MinCapacity");
				datasourceConnectionPoolTabTO.setDsUrl(URL);
				datasourceConnectionPoolTabTO.setDsUsername(dbusername);
				datasourceConnectionPoolTabTO.setDsDriverName(DriverName);
				datasourceConnectionPoolTabTO.setXaEnabled(isXAEnabled);
				datasourceConnectionPoolTabTO.setInitialCapacity(initialCapacity);
				datasourceConnectionPoolTabTO.setMaxCapacity(maxCapacity);
				datasourceConnectionPoolTabTO.setMinCapacity(minCapacity);

				/*
				 * System.out.println("Connections Total Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "ConnectionsTotalCount"));
				 * System.out.println("Current Capacity = " +
				 * mBeanConn.getAttribute(connectionPools[x], "CurrCapacity"));
				 * System.out.println("Active Connections Average Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "ActiveConnectionsAverageCount"));
				 * System.out.println("Active Connections Current Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "ActiveConnectionsCurrentCount"));
				 * System.out.println("Active Connections High Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "ActiveConnectionsHighCount"));
				 * System.out.println( "Connection Delay Time = " +
				 * mBeanConn.getAttribute(connectionPools[x], "ConnectionDelayTime"));
				 * System.out.println("Curr Capacity High Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "CurrCapacityHighCount"));
				 * System.out.println("Failed Reserve Request Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "FailedReserveRequestCount"));
				 * System.out.println("Highest Number Available = " +
				 * mBeanConn.getAttribute(connectionPools[x], "HighestNumAvailable"));
				 * System.out.println("Leaked Connection Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "LeakedConnectionCount"));
				 * 
				 * System.out.println("Number of idle DB Connections = " +
				 * mBeanConn.getAttribute(connectionPools[x], "NumAvailable"));
				 * System.out.println("Number of connections used by applications = " +
				 * mBeanConn.getAttribute(connectionPools[x], "NumUnavailable"));
				 * System.out.println("State = " + mBeanConn.getAttribute(connectionPools[x],
				 * "State")); System.out.println("Waiting For Connection Current Count = " +
				 * mBeanConn.getAttribute(connectionPools[x],
				 * "WaitingForConnectionCurrentCount"));
				 * System.out.println("Waiting For Connection Failure Total = " +
				 * mBeanConn.getAttribute(connectionPools[x],
				 * "WaitingForConnectionFailureTotal"));
				 * System.out.println("Waiting For Connection High Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "WaitingForConnectionHighCount"));
				 * System.out.println("Waiting For Connection Success Total = " +
				 * mBeanConn.getAttribute(connectionPools[x],
				 * "WaitingForConnectionSuccessTotal"));
				 * System.out.println("Waiting For Connection Total = " +
				 * mBeanConn.getAttribute(connectionPools[x], "WaitingForConnectionTotal"));
				 * System.out.println("Wait Seconds High Count = " +
				 * mBeanConn.getAttribute(connectionPools[x], "WaitSecondsHighCount"));
				 */

				datasourceTO.setDatasourceConnectionPoolTabTO(datasourceConnectionPoolTabTO);
				datasourceTO.setDatasourceGeneralTabTO(datasourceGeneralTabTO);
				datasourceTO.setDatasourceTargetTO(dsTargetTo);
				lstDatasourceTO.add(datasourceTO);
				datasourceGeneralTabTO = new DatasourceGeneralTabTO();
				datasourceConnectionPoolTabTO = new DatasourceConnectionPoolTabTO();
				datasourceTO = new DatasourceTO();
				dsTargetTo = new DatasourceTargetTO();

			}

		}
		datasourceResponseTo.setLstDatasourceTO(lstDatasourceTO);
		if (mBeanConn != null) {
			jmxConnector.close();
			System.out.println("Close Connection");
		}
		return datasourceResponseTo;
	}

	public Boolean doDataSourceOperation(@WebParam(name = "ServerConsoleInfo") ServerConsoleTO serverConsoleTO,
			@WebParam(name = "DatasourceName") String datasourceName, @WebParam(name = "Operation") String Operation)
			throws Exception {
		boolean isOperationSuccessful=false;
		try {
			ObjectName jdbcSystemResource = new ObjectName("com.bea:Name=" + datasourceName + ",Type=JDBCSystemResource");
			Map jmxConnectionMap = JMXConection.initConnection(serverConsoleTO);
			MBeanServerConnection mBeanConn = (MBeanServerConnection) jmxConnectionMap.get("MBeanServerConnection");
			JMXConnector jmxConnector = (JMXConnector) jmxConnectionMap.get("JMXConnector");
			ObjectName JDBCResource = (ObjectName) mBeanConn.getAttribute(jdbcSystemResource, "JDBCResource");
			ObjectName[] serverRT = DatasourceUtil.getServerRuntimes(mBeanConn);
			String dsName = "";
			for (int i = 0; i < serverRT.length; i++) {
				System.out.println("Server Instance=" + serverRT[i]);
				String serverName = (String) mBeanConn.getAttribute(serverRT[i], "Name");
				System.out.println("serverName=" + serverName);
				ObjectName[] connectionPools = (ObjectName[]) mBeanConn
						.getAttribute(
								new ObjectName("com.bea:Name=" + serverName + ",ServerRuntime=" + serverName
										+ ",Location=" + serverName + ",Type=JDBCServiceRuntime"),
								"JDBCDataSourceRuntimeMBeans");
				int pool_length = (int) connectionPools.length;
				for (int x = 0; x < pool_length; x++) {
					JDBCDataSourceRuntimeMBean jdbcDataSourceRuntimeMBean = (JDBCDataSourceRuntimeMBean) MBeanServerInvocationHandler
							.newProxyInstance(mBeanConn, connectionPools[x]);
					System.out.println("Name = " + jdbcDataSourceRuntimeMBean.getName());
					if (jdbcDataSourceRuntimeMBean.getName().equals(datasourceName)) {
						if (Operation != null && Operation.equalsIgnoreCase("FORCESHUTDOWN")) {
							jdbcDataSourceRuntimeMBean.forceShutdown();
						} else if (Operation != null && Operation.equalsIgnoreCase("SHUTDOWN")) {
							jdbcDataSourceRuntimeMBean.shutdown();
						} else if (Operation != null && Operation.equalsIgnoreCase("FORCESUSPEND")) {
							jdbcDataSourceRuntimeMBean.forceSuspend();
						} else if (Operation != null && Operation.equalsIgnoreCase("SUSPEND")) {
							jdbcDataSourceRuntimeMBean.suspend();
						} else if (Operation != null && Operation.equalsIgnoreCase("RESUME")) {
							jdbcDataSourceRuntimeMBean.resume();
						} else if (Operation != null && Operation.equalsIgnoreCase("START")) {
							jdbcDataSourceRuntimeMBean.start();
						}
						isOperationSuccessful=true;
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isOperationSuccessful;

	}

	public static void main(String[] args) {
		String hostname = "192.168.56.101";
		String portString = "7001";
		String username = "weblogic";
		String password = "welcome1";
		ServerConsoleTO serverConsoleTO = new ServerConsoleTO();
		serverConsoleTO.setHostname(hostname);
		serverConsoleTO.setPortString(portString);
		serverConsoleTO.setUsername(username);
		serverConsoleTO.setPassword(password);
		DatasourceMonitorServiceImpl datasourceMonitorServiceImpl = new DatasourceMonitorServiceImpl();

		try {
			ObjectName jdbcSystemResource = new ObjectName("com.bea:Name=" + "HRds" + ",Type=JDBCSystemResource");
			Map jmxConnectionMap = JMXConection.initConnection(serverConsoleTO);
			MBeanServerConnection mBeanConn = (MBeanServerConnection) jmxConnectionMap.get("MBeanServerConnection");
			JMXConnector jmxConnector = (JMXConnector) jmxConnectionMap.get("JMXConnector");
			ObjectName JDBCResource = (ObjectName) mBeanConn.getAttribute(jdbcSystemResource, "JDBCResource");
			ObjectName[] serverRT = DatasourceUtil.getServerRuntimes(mBeanConn);
			String dsName = "";
			for (int i = 0; i < serverRT.length; i++) {
				System.out.println("Server Instance=" + serverRT[i]);
				String serverName = (String) mBeanConn.getAttribute(serverRT[i], "Name");
				System.out.println("serverName=" + serverName);
				ObjectName[] connectionPools = (ObjectName[]) mBeanConn
						.getAttribute(
								new ObjectName("com.bea:Name=" + serverName + ",ServerRuntime=" + serverName
										+ ",Location=" + serverName + ",Type=JDBCServiceRuntime"),
								"JDBCDataSourceRuntimeMBeans");
				String jdbcDataSourceParams = (String) mBeanConn.getAttribute(JDBCResource,
						"Applicationname");
				/*int pool_length = (int) connectionPools.length;
				for (int x = 0; x < pool_length; x++) {
					System.out.println("Name = " + connectionPools[x]);
					JDBCDataSourceRuntimeMBean jdbcDataSourceRuntimeMBean = (JDBCDataSourceRuntimeMBean) MBeanServerInvocationHandler
							.newProxyInstance(mBeanConn, connectionPools[x]);
					
					
					//
				}*/
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * JMXWrapperRemote myJMXWrapperRemote = new JMXWrapperRemote(); try {
		 * myJMXWrapperRemote.connectToAdminServer(true,true,"weblogic", "welcome1",
		 * "t3://192.168.56.101:7001");
		 * DatasourceUtil.doDataSourceOperation(myJMXWrapperRemote, "HRds", "shutdown");
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
	}

}
