package com.oracle.jmx.ws.soap.servicebus;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jws.WebService;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;

import com.bea.wli.config.Ref;
import com.bea.wli.sb.management.configuration.ALSBConfigurationMBean;
import com.bea.wli.sb.management.configuration.BusinessServiceConfigurationMBean;
import com.bea.wli.sb.management.configuration.ProxyServiceConfigurationMBean;
import com.bea.wli.sb.management.configuration.SessionManagementMBean;
import com.oracle.jmx.eo.request.core.ServerConsoleTO;
import com.oracle.jmx.eo.request.servicebus.state.ServicebusStateTO;
import com.oracle.jmx.eo.response.servicebus.components.ServicebusComponentsTO;
import com.oracle.jmx.eo.response.servicebus.components.ServicebusResponseTO;
import com.oracle.jmx.util.common.JMXConection;
import com.oracle.jmx.util.common.JMXUtil;
import com.oracle.jmx.util.common.OSBUtil;

import weblogic.management.mbeanservers.domainruntime.DomainRuntimeServiceMBean;
import weblogic.management.runtime.ServerRuntimeMBean;

@WebService(endpointInterface = "com.oracle.jmx.ws.soap.servicebus.IServicebusManagedService")
public class ServicebusManagedServiceImpl implements IServicebusManagedService {

	@Override
	public ServicebusResponseTO getServiceBusArtifacts(ServerConsoleTO serverConsoleTO) throws Exception {
		// TODO Auto-generated method stub
		ServicebusResponseTO servicebusResponseTO = new ServicebusResponseTO();
		ServicebusComponentsTO servicebusComponentsTO = new ServicebusComponentsTO();
		Map jmxConnectionMap = JMXConection.initConnection(serverConsoleTO);
		MBeanServerConnection mBeanConn=(MBeanServerConnection) jmxConnectionMap.get("MBeanServerConnection");
		JMXConnector jmxConnector =(JMXConnector) jmxConnectionMap.get("JMXConnector");
		DomainRuntimeServiceMBean domainRuntimeServiceMBean = (DomainRuntimeServiceMBean) JMXUtil
				.findDomainRuntimeServiceMBean(mBeanConn);

		ServerRuntimeMBean[] serverRuntimes = domainRuntimeServiceMBean.getServerRuntimes();
		ALSBConfigurationMBean alsbConfigurationMBean = (ALSBConfigurationMBean) domainRuntimeServiceMBean
				.findService(ALSBConfigurationMBean.NAME, ALSBConfigurationMBean.TYPE, null);

		Set<Ref> refs = alsbConfigurationMBean.getRefs(Ref.DOMAIN);
		List<ServicebusComponentsTO> lstservicebusartifactsTO = new ArrayList();
		String sessionName =
	            "OSBServiceState_script_" + System.currentTimeMillis();
		SessionManagementMBean sessionManagementMBean = null;
		sessionManagementMBean =
                (SessionManagementMBean)domainRuntimeServiceMBean.findService(SessionManagementMBean.NAME,
                                                                              SessionManagementMBean.TYPE,
                                                                              null);
        sessionManagementMBean.createSession(sessionName);
		for (Ref ref : refs) {
			String typeId = ref.getTypeId();
			String fullServicePath=ref.getFullName();
			
			if (typeId != null && typeId.equals("ProxyService") ) {
				System.out.println(ProxyServiceConfigurationMBean.NAME +"." +sessionName);
				ProxyServiceConfigurationMBean proxyServiceConfigurationMBean =
	                    (ProxyServiceConfigurationMBean)domainRuntimeServiceMBean.findService(ProxyServiceConfigurationMBean.NAME + "." + sessionName,ProxyServiceConfigurationMBean.TYPE, null);
				servicebusComponentsTO.setApplicationName(ref.getProjectName());
				servicebusComponentsTO.setServiceName(ref.getLocalName());
				servicebusComponentsTO.setServiceType(ref.getTypeId());				
				boolean proxyStatus=proxyServiceConfigurationMBean.isEnabled(ref);				
				if(proxyStatus){
					servicebusComponentsTO.setServiceStatus("Enabled");
				}else {
					servicebusComponentsTO.setServiceStatus("Disabled");
				}
				servicebusComponentsTO.setFullServicePath(fullServicePath);
				lstservicebusartifactsTO.add(servicebusComponentsTO);
				servicebusComponentsTO = new ServicebusComponentsTO();
			}else if(typeId != null && typeId.equals("BusinessService")) {
				  BusinessServiceConfigurationMBean businessServiceConfigurationMBean =
		                    (BusinessServiceConfigurationMBean)domainRuntimeServiceMBean.findService(BusinessServiceConfigurationMBean.NAME +
		                                                                                             "." +
		                                                                                             sessionName,
		                                                                                             BusinessServiceConfigurationMBean.TYPE,
		                                                                                             null);
				boolean bsStatus=businessServiceConfigurationMBean.isEnabled(ref);
				servicebusComponentsTO.setApplicationName(ref.getProjectName());
				servicebusComponentsTO.setServiceName(ref.getLocalName());
				servicebusComponentsTO.setServiceType(ref.getTypeId());
				servicebusComponentsTO.setFullServicePath(fullServicePath);
				if(bsStatus){
					servicebusComponentsTO.setServiceStatus("Enabled");
				}else {
					servicebusComponentsTO.setServiceStatus("Disabled");
				}
				lstservicebusartifactsTO.add(servicebusComponentsTO);
				servicebusComponentsTO = new ServicebusComponentsTO();
			}
		}
		sessionManagementMBean.discardSession(sessionName);
		servicebusResponseTO.setLstservicebusartifactsTO(lstservicebusartifactsTO);
		if(mBeanConn!=null) {
			jmxConnector.close();
			System.out.println("Close Connection");
		}
		return servicebusResponseTO;
	}
	
	@Override
	public Boolean changeOSBServiceState(ServerConsoleTO serverConsoleTO,ServicebusStateTO  servicebusstateTO) throws MalformedURLException, IOException {
		Map jmxConnectionMap = JMXConection.initConnection(serverConsoleTO);
		MBeanServerConnection mBeanConn=(MBeanServerConnection) jmxConnectionMap.get("MBeanServerConnection");
		JMXConnector jmxConnector =(JMXConnector) jmxConnectionMap.get("JMXConnector");
    	Boolean isStateChanged=false; 
        SessionManagementMBean sessionManagementMBean = null;
        String sessionName =
            "OSBServiceState_script_" + System.currentTimeMillis();
        String servicetype;
        String serviceURI;
        String action;
        String description = "";
 
 
        try {
 
            
            servicetype = servicebusstateTO.getServiceType();
            serviceURI = servicebusstateTO.getServiceURI();
            action = servicebusstateTO.getAction();
 
            DomainRuntimeServiceMBean domainRuntimeServiceMBean = (DomainRuntimeServiceMBean) JMXUtil
    				.findDomainRuntimeServiceMBean(mBeanConn);

            // Create a session via SessionManagementMBean.
            sessionManagementMBean =
                    (SessionManagementMBean)domainRuntimeServiceMBean.findService(SessionManagementMBean.NAME,
                                                                                  SessionManagementMBean.TYPE,
                                                                                  null);
            sessionManagementMBean.createSession(sessionName);
 
            if (servicetype.equalsIgnoreCase("ProxyService")) {
 
                // A Ref uniquely represents a resource, project or folder that is managed by the Configuration Framework.
                // A Ref object has two components: A typeId that indicates whether it is a project, folder, or a resource, and an array of names of non-zero length.
                // For a resource the array of names start with the project name, followed by folder names, and end with the resource name.
                // For a project, the Ref object simply contains one name component, that is, the project name.
                // A Ref object for a folder contains the project name followed by the names of the folders which it is nested under.
                Ref ref = OSBUtil.constructRef("ProxyService", serviceURI);
 
                ProxyServiceConfigurationMBean proxyServiceConfigurationMBean =
                    (ProxyServiceConfigurationMBean)domainRuntimeServiceMBean.findService(ProxyServiceConfigurationMBean.NAME +
                                                                                          "." +
                                                                                          sessionName,
                                                                                          ProxyServiceConfigurationMBean.TYPE,
                                                                                          null);
                
                if (action.equalsIgnoreCase("Enable")) {
                    proxyServiceConfigurationMBean.enableService(ref);
                    description = "Enabled the service: " + serviceURI;
                    System.out.print("Enabling service " + serviceURI);
                } else if (action.equalsIgnoreCase("Disable")) {
                    proxyServiceConfigurationMBean.disableService(ref);
                    description = "Disabled the service: " + serviceURI;
                    System.out.print("Disabling service " + serviceURI);
                } else {
                    System.out.println("Unsupported value for ACTION");
                }
            } else if (servicetype.equals("BusinessService")) {
                Ref ref = OSBUtil.constructRef("BusinessService", serviceURI);
 
                BusinessServiceConfigurationMBean businessServiceConfigurationMBean =
                    (BusinessServiceConfigurationMBean)domainRuntimeServiceMBean.findService(BusinessServiceConfigurationMBean.NAME +
                                                                                             "." +
                                                                                             sessionName,
                                                                                             BusinessServiceConfigurationMBean.TYPE,
                                                                                             null);
                if (action.equalsIgnoreCase("Enable")) {
                    businessServiceConfigurationMBean.enableService(ref);
                    description = "Enabled the service: " + serviceURI;
                    System.out.print("Enabling service " + serviceURI);
                } else if (action.equalsIgnoreCase("Disable")) {
                    businessServiceConfigurationMBean.disableService(ref);
                    description = "Disabled the service: " + serviceURI;
                    System.out.print("Disabling service " + serviceURI);
                } else {
                    System.out.println("Unsupported value for ACTION");
                }
            }
            sessionManagementMBean.activateSession(sessionName, description);
            isStateChanged=true;
            System.out.println(" has been succesfully completed");
        } catch (Exception ex) {
            if (sessionManagementMBean != null) {
                try {
                   sessionManagementMBean.discardSession(sessionName);
                   isStateChanged=false;
                    System.out.println(" resulted in an error.");
                } catch (Exception e) {
                    System.out.println("Unable to discard session: " +
                                       sessionName);
                }
            }
 
            ex.printStackTrace();
        } finally {
            if (mBeanConn != null)
                try {
                	jmxConnector.close();
                	System.out.println("Close Connection");
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return isStateChanged;
    }
 

	public static void main(String[] args) {
		String hostname = "192.168.56.101";
		String portString = "7001";
		String username = "weblogic";
		String password = "welcome1";
		ServerConsoleTO serverConsoleTO = new ServerConsoleTO();
		serverConsoleTO.setHostname(hostname);
		serverConsoleTO.setPortString(portString);
		serverConsoleTO.setUsername(username);
		serverConsoleTO.setPassword(password);
		ServicebusManagedServiceImpl servicebusMonitorServiceImpl = new ServicebusManagedServiceImpl();
		try {
			ServicebusResponseTO servicebusResponseTO = servicebusMonitorServiceImpl
					.getServiceBusArtifacts(serverConsoleTO);

		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
