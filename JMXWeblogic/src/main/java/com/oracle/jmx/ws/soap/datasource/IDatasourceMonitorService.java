package com.oracle.jmx.ws.soap.datasource;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.oracle.jmx.eo.request.core.ServerConsoleTO;
import com.oracle.jmx.eo.response.datasource.DatasourceResponseTO;

@WebService
public interface IDatasourceMonitorService {
	@WebMethod
	public DatasourceResponseTO getDataSources(@WebParam(name="ServerConsoleInfo")ServerConsoleTO serverConsoleTO) throws Exception;
	@WebMethod
	public Boolean doDataSourceOperation(@WebParam(name="ServerConsoleInfo")ServerConsoleTO serverConsoleTO,@WebParam(name="DatasourceName") String datasourceName,@WebParam(name="Operation") String Operation) throws Exception;
	
}
