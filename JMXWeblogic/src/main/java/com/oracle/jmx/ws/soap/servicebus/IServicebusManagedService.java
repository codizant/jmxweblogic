package com.oracle.jmx.ws.soap.servicebus;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.oracle.jmx.eo.request.core.ServerConsoleTO;
import com.oracle.jmx.eo.request.servicebus.state.ServicebusStateTO;
import com.oracle.jmx.eo.response.servicebus.components.ServicebusResponseTO;
@WebService
public interface IServicebusManagedService {
	@WebMethod
	public ServicebusResponseTO getServiceBusArtifacts(@WebParam(name="ServerConsoleInfo") ServerConsoleTO serverConsoleTO) throws Exception ;
	@WebMethod
	public Boolean changeOSBServiceState(@WebParam(name="ServerConsoleInfo") ServerConsoleTO serverConsoleTO,@WebParam(name="ServicebusStateInfo") ServicebusStateTO  servicebusstateTO) throws MalformedURLException, IOException;
}
