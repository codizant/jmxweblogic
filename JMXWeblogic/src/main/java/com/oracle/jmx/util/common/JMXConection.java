package com.oracle.jmx.util.common;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

import com.oracle.jmx.eo.request.core.ServerConsoleTO;



public class JMXConection {
	
public static Map initConnection(ServerConsoleTO serverConsoleTO) throws IOException, MalformedURLException {
		Map jmxConnectionMap=new HashMap();
		MBeanServerConnection connection=null;
		JMXConnector jmxConnector;
		String protocol = "t3";
		Integer portInteger = Integer.valueOf(serverConsoleTO.getPortString());
		int port = portInteger.intValue();
		String jndiroot = "/jndi/";
		String mserver = "weblogic.management.mbeanservers.domainruntime";
		JMXServiceURL serviceURL = new JMXServiceURL(protocol, serverConsoleTO.getHostname(), port, jndiroot + mserver);

		Hashtable h = new Hashtable();
		h.put(Context.SECURITY_PRINCIPAL, serverConsoleTO.getUsername());
		h.put(Context.SECURITY_CREDENTIALS, serverConsoleTO.getPassword());
		h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
		h.put("jmx.remote.x.request.waiting.timeout", new Long(10000));
		jmxConnector = JMXConnectorFactory.connect(serviceURL, h);
		connection = jmxConnector.getMBeanServerConnection();
		jmxConnectionMap.put("MBeanServerConnection", connection);
		jmxConnectionMap.put("JMXConnector", jmxConnector);
		return jmxConnectionMap;
	}

}
