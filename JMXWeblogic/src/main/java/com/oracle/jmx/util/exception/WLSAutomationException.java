package com.oracle.jmx.util.exception;

public class WLSAutomationException extends Exception {

	static final long serialVersionUID = 12876523842L;
	
	/**
	 * 
	 */
	public WLSAutomationException() {
	}

	/**
	 * @param message
	 */
	public WLSAutomationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public WLSAutomationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public WLSAutomationException(String message, Throwable cause) {
		super(message, cause);
	}

}