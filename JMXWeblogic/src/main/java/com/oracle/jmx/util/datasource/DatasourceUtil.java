package com.oracle.jmx.util.datasource;

import java.io.IOException;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import com.oracle.jmx.util.common.JMXWrapperRemote;
import com.oracle.jmx.util.exception.WLSAutomationException;

public class DatasourceUtil {

	public static ObjectName getJDBCSystemProperties(MBeanServerConnection mBeanConn, ObjectName objectName)
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException,
			IOException {
		return (ObjectName) getObjectName(mBeanConn, objectName, "Properties");
	}

	public static Object getObjectName(MBeanServerConnection mBeanConn, ObjectName objectName, String attributeName)
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException,
			IOException {
		return mBeanConn.getAttribute(objectName, attributeName);
	}

	public static ObjectName getJDBCUserPropertyBean(MBeanServerConnection mBeanConn, ObjectName jdbcPropertiesBean)
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException,
			IOException, MalformedObjectNameException, NullPointerException, InstanceAlreadyExistsException,
			NotCompliantMBeanException {
		ObjectName jdbcPropertyBean = null;
		ObjectName[] jdbcPropertyBeans = (ObjectName[]) getObjectName(mBeanConn, jdbcPropertiesBean, "Properties");
		for (int j = 0; j < jdbcPropertyBeans.length; j++) {
			jdbcPropertyBean = jdbcPropertyBeans[j];
			String jdbcPropertyName = (String) getObjectName(mBeanConn, jdbcPropertyBean, "Name");
			if ("user".equalsIgnoreCase(jdbcPropertyName)) {
				break;
			}
		}
		return jdbcPropertyBean;
	}

	public static ObjectName[] getListDS(String serverName, MBeanServerConnection mBeanConn) {
		ObjectName[] listDataSources = null;
		try {
			String service = "com.bea:ServerRuntime=" + serverName + ",Name=" + serverName + ",Location=" + serverName
					+ ",Type=JDBCServiceRuntime";
			ObjectName temp = new ObjectName(service);
			listDataSources = (ObjectName[]) mBeanConn.getAttribute(temp, "JDBCDataSourceRuntimeMBeans");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return listDataSources;

	}

	public static ObjectName[] getServerRuntimes(MBeanServerConnection mBeanConn) throws Exception {
		ObjectName service = new ObjectName("com.bea:Name=DomainRuntimeService,Type=weblogic.management."
				+ "mbeanservers.domainruntime.DomainRuntimeServiceMBean");
		return (ObjectName[]) mBeanConn.getAttribute(service, "ServerRuntimes");
	}

	public static void doDataSourceOperation(JMXWrapperRemote myJMXWrapper, String datasourcename, String operationName)  throws WLSAutomationException{
		 try  {
		      ObjectName myDomainMBean = myJMXWrapper.getDomainConfigRoot();
		      ObjectName mySystemResourceMBean = (ObjectName)myJMXWrapper.invoke(myDomainMBean,"lookupJDBCSystemResource",new Object[]{new String(datasourcename)}, new String[]{String.class.getName()});
		      if (mySystemResourceMBean!=null) {
		            ObjectName myDataResourceMBean = (ObjectName)myJMXWrapper.getAttribute(mySystemResourceMBean, "JDBCResource");
		            myJMXWrapper.invoke(myDataResourceMBean,operationName,new Object[]{},new String[]{});
		      }
		      else
		            throw new WLSAutomationException("Datasource "+datasourcename+" does not exist");
		   }
		   catch(Exception ex) {
		       throw new WLSAutomationException(ex);
		   }
	}
}
