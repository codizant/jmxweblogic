package com.oracle.jmx.util.common;

import com.bea.wli.config.Ref;

public class OSBUtil {
	public static Ref constructRef(String refType, String serviceURI) {
        Ref ref = null;
        String[] uriData = serviceURI.split("/");
        ref = new Ref(refType, uriData);
        return ref;
    }
 
}
